#!/usr/bin/make -f

# generate documentation unless nodoc requested
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
DOCS = $(foreach f,README CONFIG PCN THEMING,$f.html $f.txt)
CHANGELOGS = CHANGELOG.html CHANGELOG.txt
endif

override_dh_clean:
	dh_clean -- $(DOCS) $(CHANGELOGS)

override_dh_auto_build: $(DOCS) $(CHANGELOGS)
	dh_auto_build

# link embedded code project to where qmake expects it
execute_after_dh_testdir:
	rm -rf submodules
	mkdir -p submodules
	ln -sfT ../Xhsluv submodules/hsluv-c

override_dh_auto_test:
	dh_auto_test

# rename to avoid clash with GTK image viewer of same name
execute_after_dh_install:
	mv debian/matrix-mirage/usr/share/applications/mirage.desktop \
		debian/matrix-mirage/usr/share/applications/matrix-mirage.desktop
	mv debian/matrix-mirage/usr/share/icons/hicolor/256x256/apps/mirage.png \
		debian/matrix-mirage/usr/share/icons/hicolor/256x256/apps/matrix-mirage.png

override_dh_installdocs:
	dh_installdocs -- $(DOCS)

override_dh_installchangelogs:
	dh_installchangelogs -- $(CHANGELOGS)

%.html:
	cmark-gfm $(if $(wildcard $*.md),$*.md,docs/$*.md) > $@

%.txt:
	cmark-gfm --to plaintext $(if $(wildcard $*.md),$*.md,docs/$*.md) > $@

%:
	dh $@
