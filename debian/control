Source: matrix-mirage
Section: net
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 cmark-gfm <!nodoc>,
 debhelper-compat (= 13),
 flake8 <!nocheck>,
 libmediainfo-dev,
 libolm-dev,
 libx11-dev,
 libxss-dev,
 mypy <!nocheck>,
 python3-aiofiles <!nocheck>,
 python3-dev,
 python3-typeshed <!nocheck>,
 qt5-qmake,
 qtdeclarative5-dev,
 qtquickcontrols2-5-dev,
Standards-Version: 4.6.0
Homepage: https://github.com/mirukana/mirage
Vcs-Git: https://salsa.debian.org/matrix-team/matrix-mirage.git
Vcs-Browser: https://salsa.debian.org/matrix-team/matrix-mirage
Rules-Requires-Root: no

Package: matrix-mirage
Architecture: any
Depends:
 python3-pil,
 python3-aiofiles,
 python3-appdirs,
 python3-cairosvg,
 python3-dbus,
 python3-magic,
 python3-hsluv,
 python3-html-sanitizer,
 python3-lxml,
 python3-matrix-nio (>= 0.18.5),
 python3-mistune0,
 python3-plyer,
 python3-pymediainfo,
 python3-redbaron,
 python3-sortedcontainers,
 python3-watchgod,
 python3 (>= 3.7) | python3-async-generator,
 python3 (>= 3.7) | python3-dataclasses,
 python3 (>= 3.8) | python3-pyfastcopy,
 qml-module-io-thp-pyotherside,
 qml-module-qtav,
 qml-module-qtgraphicaleffects,
 qml-module-qt-labs-folderlistmodel,
 qml-module-qt-labs-platform,
 qml-module-qt-labs-qmlmodels,
 qml-module-qtquick2,
 qml-module-qtquick-controls2,
 qml-module-qtquick-dialogs,
 qml-module-qtquick-layouts,
 qml-module-qtquick-shapes,
 qml-module-qtquick-window2,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 fonts-hack,
 fonts-roboto-unhinted,
 qt5-image-formats-plugins,
Description: desktop IM client for the Matrix protocol
 Mirage is a fancy, customizable, keyboard-operable chat client
 for the Matrix protocol,
 written in Qt/QML and Python.
 .
 Notable Matrix features supported:
  * Markdown parsing when sending messages
  * End-to-end encryption
  * Multiple concurrent accounts
  * Session administration
  * Room administration
 .
 Notable Matrix features missing:
  * Communities (a.k.a. groups of rooms)
  * Audio/video chat
 .
 Matrix is an open, federated communications protocol.
